package cn.kingteng.staro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StaroApplication {

    public static void main(String[] args) {
        SpringApplication.run(StaroApplication.class, args);
    }

}
